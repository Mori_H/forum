package forum.service;

import static forum.utils.CloseableUtil.*;
import static forum.utils.DBUtil.*;

import java.sql.Connection;

import forum.beans.User;
import forum.dao.UserDao;
import forum.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}