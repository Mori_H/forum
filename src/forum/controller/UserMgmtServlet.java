package forum.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/userMgmt")
public class UserMgmtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("userMgmt.jsp").forward(request, response);


//
//	HttpSession session = request.getSession();
//    //セッションよりログインユーザーの情報を取得
//    User loginUser = (User) session.getAttribute("loginUser");
//    //ログインユーザー情報のidを元にDBからユーザー情報取得
//    User editUser = new UserService().getUser(loginUser.getId());
//    request.setAttribute("editUser", editUser);

    request.getRequestDispatcher("settings.jsp").forward(request, response);
}
}